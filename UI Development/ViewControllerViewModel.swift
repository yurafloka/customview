//
//  ViewControllerViewModel.swift
//  UI Development
//
//  Created by Yurii Tsymbala on 7/23/18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import Foundation

class ViewControllerViewModel {

  func getCircleViewModel() -> CircleSegmentViewModel {
    let segment1 = CircleSegment(color: .black, percent: 0.2, borderWidth: 8.0)
    let segment2 = CircleSegment(color: .green, percent: 0.3, borderWidth: 22.0)
    let segment3 = CircleSegment(color: .red, percent: 0.4, borderWidth: 16.0)
    let segmentsArray = [segment1, segment2, segment3]
    let viewModel = CircleSegmentViewModel(segments: segmentsArray,
                                    borderWidth: 8,
                                    borderColor: .lightGray,
                                    fillColor: .clear,
                                    startPoint: .top)
    return viewModel
  }
}



//
//  CircleViewModel.swift
//  UI Development
//
//  Created by Yurii Tsymbala on 7/23/18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import UIKit


struct CircleSegmentViewModel {
  let segments: [CircleSegment]
  let borderWidth: CGFloat
  let borderColor: UIColor
  let fillColor: UIColor
  let startPoint: CircleStartAngle
}

struct CircleSegment {
  let color: UIColor
  let percent: CGFloat
  let borderWidth: CGFloat
}

enum CircleStartAngle: CGFloat {
  case top = -90
  case bottom = 90
  case left = 180
  case right = 0
}




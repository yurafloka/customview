//
//  MyCustomView.swift
//  UI Development
//
//  Created by Yurii Tsymbala on 7/19/18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import UIKit
@IBDesignable
class CircleSegmentedView: UIView {

  private struct Constants {
    static let borderWidth: CGFloat = 8
    static let borderColor: UIColor = .lightGray
    static let fillColor: UIColor = .clear
    static let animationDuration: Double = 0.5
    static let maximumDegree: CGFloat = 360
    static let clockwise = true
  }

  private var radius: CGFloat {
    return bounds.width / 2 - Constants.borderWidth / 2
  }

  private var arcCenter: CGPoint {
    return CGPoint(x: bounds.width / 2, y: bounds.height / 2)
  }

  private var lastSegmentEndPoint: CGFloat = 0

  override init(frame: CGRect) {
    super.init(frame: frame)
    drawCircle(startAngle: CircleStartAngle.top.rawValue, endAngle: Constants.maximumDegree - abs(CircleStartAngle.top.rawValue),
         lineWidth: Constants.borderWidth,
         strokeColor: Constants.borderColor.cgColor, fillColor: Constants.fillColor.cgColor,
         animated: true)
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder )

    drawCircle(startAngle: CircleStartAngle.top.rawValue, endAngle: Constants.maximumDegree - abs(CircleStartAngle.top.rawValue),
         lineWidth: Constants.borderWidth,
         strokeColor: Constants.borderColor.cgColor, fillColor: Constants.fillColor.cgColor,
         animated: true)
  }

  func bind(_ viewModel: CircleSegmentViewModel) {
    cleanSubLayers()

    drawCircle(startAngle: CircleStartAngle.top.rawValue, endAngle: Constants.maximumDegree - abs(CircleStartAngle.top.rawValue),
         lineWidth: viewModel.borderWidth, strokeColor: Constants.borderColor.cgColor,
         fillColor: Constants.fillColor.cgColor,
         animated: true)

    lastSegmentEndPoint = viewModel.startPoint.rawValue

    for segment in viewModel.segments {
      let endAngle = percentsToAngle(segment.percent) + lastSegmentEndPoint
      drawCircle(startAngle: lastSegmentEndPoint,
           endAngle: endAngle,
           lineWidth: segment.borderWidth,
           strokeColor: segment.color.cgColor,
           fillColor: UIColor.clear.cgColor,
           animated: true)
      lastSegmentEndPoint = endAngle
    }
  }

  private func drawCircle(startAngle: CGFloat, endAngle: CGFloat, lineWidth: CGFloat,
                    strokeColor: CGColor, fillColor: CGColor, animated: Bool) {
    let circleBezierPath = UIBezierPath(arcCenter: arcCenter,
                                        radius: radius,
                                        startAngle: degreesToRadians(startAngle),
                                        endAngle: degreesToRadians(endAngle),
                                        clockwise: Constants.clockwise)

    let circleLayer = CAShapeLayer()
    circleLayer.path = circleBezierPath.cgPath
    circleLayer.lineWidth = lineWidth
    circleLayer.strokeColor = strokeColor
    circleLayer.fillColor = fillColor

    if animated == true {
      let animation = CABasicAnimation(keyPath: "strokeEnd")
      animation.fromValue = 0
      animation.toValue = 1
      animation.duration = Constants.animationDuration
      circleLayer.add(animation, forKey: nil)
    }

    layer.addSublayer(circleLayer)
  }

  private func cleanSubLayers() {
    layer.sublayers?.forEach({ layer in
      layer.removeFromSuperlayer()
    })
  }

  private func degreesToRadians(_ degrees: CGFloat) -> CGFloat {
    return CGFloat(degrees) * CGFloat.pi / 180
  }

  private func percentsToAngle(_ percents: CGFloat) -> CGFloat {
    return 360 * percents
  }
}

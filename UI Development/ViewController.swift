//
//  ViewController.swift
//  UI Development
//
//  Created by Yurii Tsymbala on 7/18/18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  @IBOutlet weak var myCustomView: CircleSegmentedView!

  var viewControllerViewModel = ViewControllerViewModel()

  override func viewDidLoad() {
    super.viewDidLoad()
    myCustomView.backgroundColor = .clear
    myCustomView.bind(viewControllerViewModel.getCircleViewModel())
  }
}
